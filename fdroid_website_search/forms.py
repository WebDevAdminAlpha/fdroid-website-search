from haystack.inputs import BaseInput, Clean, Not, Clean, Exact
from haystack.forms import SearchForm
from haystack.query import SQ

from .search_indexes import SEARCH_FIELDS


#class FDroidSearchForm(SearchForm):
#
#    def search(self):
#
#        if not self.is_valid():
#            return self.no_query_found()
#
#        if not self.cleaned_data.get('q'):
#            return self.no_query_found() 
#
#        q = self.cleaned_data.get('q')
#        sq = SQ()
#        for field in SEARCH_FIELDS:
#            sq.add(SQ(**{field: q}), SQ.OR)
#
#        sqs = self.searchqueryset.filter(sq)
#
#        if self.load_all:
#            sqs = sqs.load_all()
#
#        return sqs

class FDroidSearchForm(SearchForm):

    def search(self):

        if not self.is_valid():
            return self.no_query_found()

        if not self.cleaned_data.get('q'):
            return self.no_query_found() 

        q = self.cleaned_data.get('q')
        sq = SQ(**{'text': q})

        sqs = self.searchqueryset.filter(sq)

        if self.load_all:
            sqs = sqs.load_all()

        return sqs
