#!/usr/bin/env python3
#
# search_indexes.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from haystack import indexes

from fdroid_website_search.models import App

SEARCH_FIELDS = ('packageName', 'summary', 'description', 'whatsNew',
                 'categories', 'antiFeatures', 'authorName', 'authorEmail',
                 'webSite', 'sourceCode', 'issueTracker', 'changelog',
                 'license', 'donate', 'bitcoin', 'litecoin', 'flattrID',
                 'liberapayID')

class AppIndex(indexes.SearchIndex, indexes.Indexable):

    # text is just empty, because haystack requires one and only one document=True item
    # but we prefer to search by individual columns
    text = indexes.EdgeNgramField(document=True, use_template=True)

    #name = indexes.NgramField(use_template=True, boost=3.0)
    #packageName = indexes.NgramField(model_attr='packageName')
    #summary = indexes.NgramField(use_template=True, boost=2.0)
    #description = indexes.CharField(use_template=True)
    #whatsNew = indexes.CharField(use_template=True)
    #categories = indexes.CharField(use_template=True)
    #antiFeatures = indexes.CharField(use_template=True)

    #authorName = indexes.CharField(model_attr='authorName')
    #authorEmail = indexes.CharField(model_attr='authorEmail')

    #webSite = indexes.CharField(model_attr='webSite')
    #sourceCode = indexes.CharField(model_attr='sourceCode')
    #issueTracker = indexes.CharField(model_attr='issueTracker')
    #changelog = indexes.CharField(model_attr='changelog')

    #license = indexes.CharField(model_attr='license')

    #donate = indexes.CharField(model_attr='donate')
    #bitcoin = indexes.CharField(model_attr='bitcoin')
    #litecoin = indexes.CharField(model_attr='litecoin')
    #flattrID = indexes.CharField(model_attr='flattrID')
    #liberapayID = indexes.CharField(model_attr='liberapayID')

    #added = indexes.DateTimeField(model_attr='added')
    #lastUpdated = indexes.DateTimeField(model_attr='lastUpdated')

    def get_model(self):
        return App
