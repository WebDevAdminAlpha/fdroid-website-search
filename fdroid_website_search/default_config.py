color_text = '#212121'
color_background = '#ffffff'
color_background_header = '#1976d2'

repos = [{'repo': 'https://f-droid.org/repo',
          'site': 'https://f-droid.org',
          'icon-mirror': 'https://fdroid.tetaneutral.net/fdroid/repo'}]

langs = ['bo', 'en', 'de', 'es', 'fa', 'fr', 'he', 'it', 'ko', 'pt_BR',
         'ru', 'tr', 'uk', 'zh_Hans', 'zh_Hant']
